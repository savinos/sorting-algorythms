require 'minitest/autorun'
require './sorter.rb'

class TestSort < Minitest::Test
  SIZE = 100
  MAX_NUM = 1000
  def setup
    @sorter = Sorter.new
    @unsorted_array = Array.new(SIZE) {
      rand(MAX_NUM)
    }
    @sorted_array = @unsorted_array.sort
  end

  def test_insertion_sort
    assert_equal @sorted_array, @sorter.insertion_sort(@unsorted_array) 
  end

  def test_selection_sort
    assert_equal @sorted_array, @sorter.selection_sort(@unsorted_array) 
  end

  def test_merge_sort
    assert_equal @sorted_array, @sorter.merge_sort(@unsorted_array) 
  end
end
