class Sorter

  # todo improve the code
  def insertion_sort(array)
    index = 1
    array_size = array.size
    while index < array_size
      movable_element_index = index
      movable_element = array[movable_element_index]
      
      element_to_compare_index = index - 1
      element_to_compare = array[element_to_compare_index]

      while element_to_compare_index >= 0
        if element_to_compare >= movable_element 
          array.insert(element_to_compare_index, array.delete_at(movable_element_index))
          movable_element_index = element_to_compare_index
          movable_element = array[movable_element_index]
        else
          break
        end
        element_to_compare_index -= 1
        element_to_compare = array[element_to_compare_index]
      end
      index += 1
    end
    array
  end

  # improve it
  def selection_sort(array)
    sorted_array = []
    unsorted_array = Array.new(array)
    for i in 1..array.size do
      sorted_array << unsorted_array.delete_at(unsorted_array.find_index(unsorted_array.min))
    end
    return sorted_array
  end

  def merge_sort(array)
    divide_and_sort_merge_sort(array[0...(array.size/2)], array[(array.size/2)...array.size])
  end

  private

  # improve it
  def divide_and_sort_merge_sort(left, right)
    sorted = []
    if left.size > 1 then
      left = divide_and_sort_merge_sort(left[0...(left.size/2)], left[(left.size/2)...left.size])
    end
    if right.size > 1 then
      right = divide_and_sort_merge_sort(right[0...(right.size/2)], right[(right.size/2)...right.size])
    end
    while !left.empty? || !right.empty? do
      if left.empty?
        sorted += right
        right = []
      elsif right.empty?
        sorted += left
        left = []
      else
        if left[0] < right[0]
          sorted << left[0]
          left.delete_at(0)
        else
          sorted << right[0]
          right.delete_at(0)
        end
      end
    end
    return sorted
  end
end
